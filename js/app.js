(function (window) {
	'use strict';



	var STORAGE_KEY = 'todos-vuejs-2.0'
	var todoStorage = {
		fetch: function () {
			var todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]')
			// todos.forEach(function (todo, index) {
			// 	todo.id = index
			// })
			// todoStorage.uid = todos.length
			return todos
		},
		save: function (todos) {
			localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
		}
	}

	var filters = {
		all: function (todos) {
			return todos
		},
		active: function (todos) {
			return todos.filter(function (todo) {
				return !todo.completed
			})
		},
		completed: function (todos) {
			return todos.filter(function (todo) {
				return todo.completed
			})
		}
	}

	var app = new Vue({
		el:".todoapp",
		data:{
			// todos:[
			// 	// {title:'第一个测试',completed:false,editing:false}
			// ],
			todos:todoStorage.fetch(),
			newTodo:'',
			// checkAll:false
			visibility:'all'
		},
		methods:{
			addTodo:function() {
				var value = this.newTodo && this.newTodo.trim()
				if (!value) {
					return
				}
				this.todos.push({title:this.newTodo,completed:false,editing:false});
				this.newTodo = '';
				// todoStorage.save(this.todos);
			},
			reverseCheckAll:function() {
				this.checkAll = !this.checkAll;
				// todoStorage.save(this.todos);
			},
			removeTodo:function(todo) {
				this.todos.splice(this.todos.indexOf(todo),1);
				// todoStorage.save(this.todos);
			},
			clearCompleted: function() {
				this.todos = filters.active(this.todos);
				// todoStorage.save(this.todos);
			},
			editTodo: function(todo) {
				todo.editing = true;
				this.beforeEditCache = todo.title;
			},
			doneEdit: function(todo) {
				todo.editing = false;

				if (!this.beforeEditCache) {
					return;
				}
				this.beforeEditCache = null
				todo.title = todo.title.trim()
				if (!todo.title) {
					this.removeTodo(todo)
				}
			},
			cancelEdit: function(todo) {
				todo.editing = false;
				todo.title = this.beforeEditCache;
				this.beforeEditCache = null;
			}
		},
		computed:{
			filteredTodos: function () {
				return filters[this.visibility](this.todos)
			},
			remaining: function () {
				return filters.active(this.todos).length;

			},
			checkAll: {
				get: function (value) {
					return this.remaining === 0
				},
				set: function (value) {
					this.todos.forEach(function (todo) {
						todo.completed = value
					})
				}
			},
		},
		// filters: {
		// 	pluralize: function (n) {
		// 		return n === 1 ? 'item' : 'items'
		// 	}
		// },
		watch: {
			todos: {
				handler: function (todos) {
					todoStorage.save(todos)
				},
				// deep: true
			}
		},
		directives: {
			'todo-focus': function(el, binding) {
				if(binding.value) {
					el.focus();
				}
			}
		}

	})

	// handle routing
	function onHashChange () {
		var visibility = window.location.hash.replace(/#\/?/, '')
		if (filters[visibility]) {
			app.visibility = visibility
		} else {
			window.location.hash = ''
			app.visibility = 'all'
		}
	}

	window.addEventListener('hashchange', onHashChange);

	onHashChange();


})(window);

